��    \      �              �     �  N   �      5     V  ;   [     �     �  !   �  !   �  /         0     8     D     W  	   \     f     s     �     �     �     �     �     �  	   �     �     �     �  	     F        T     q     �     �  	   �  5   �     �     �     	     #	     1	     A	  	   J	     T	     ]	  1   n	     �	  ,   �	     �	     �	     
  +   4
     `
  (   x
     �
     �
  "   �
  %   �
        
   <     G     O     X     d     t     �     �     �     �  
   �     �  A   �      %     F     ]     f  	   m  	   w     �     �     �     �     �     �     �       z   9     �     �  
   �  	   �  	     �    	   �  B   �     �     	  6        G     W     g     �  $   �     �     �     �     �     �               /     <     U     b     o          �     �     �     �     �  <   �               ;     B     I  0   P     �     �     �     �     �     �     �          	  %        <  -   X     �     �     �  '   �     �  &         '     =     P  $   j     �     �     �     �     �     �     �     �               >     E     L  9   S  $   �     �     �     �  	   �  	   �     �     �          &     9     L     _     x  Z   �     �          '     4     ;   About me Alternatively, you can paste the following link in your browser's address bar: An unexpected error has occurred Back Check your email for the instruction to reset your password Click to Register! Click to Reset It Congratulations, post is created! Congratulations, post is updated! Congratulations, you are now a registered user! Content Create Post Dear %(username)s, Edit Edit Post Edit Profile Edit your profile Email Email address is must Explore Explore Page File Not Found Follow Forbidden Forgot Your Password? Hi, %(username)s! Home Home Page If you have not requested a password reset simply ignore this message. Invalid username or password Last seen on: %(when)s Login Logout Microblog Must be at least 6 characters, at most 64 characters. Must be at most 140 characters Must be at most 64 characters Need Post title. Need password Need user name. New Post New User? Password Password is must Please input User name between 6 ~ 64 characters. Please input email address Please input email address under 120 length. Please input password Please input password again Please input post content Please input post content under 140 length. Please input post title Please input post title under 64 length. Please input self-content Please input user name Please log in to access this page! Please use a different email address. Please use a different username. Post Title Profile Register Remember Me Repeat Password Request Password Reset Reset Password Reset Your Password Send Password Reset e-mail Sign In Sincerely, Submit The administrator has been notified. Sorry for the inconvenience! There is no user with email, {0} To reset your password Unfollow Update User Name User name User name is must User {0} not found. User: %(username)s Welcome to Microblog You are following {0} You are not following {0} You cannot follow yourself! You cannot unfollow yourself! You don't have the permission to access the requested resource. It is either read-protected or not readable by the
server. Your changes have been saved. Your password has been reset. click here followers following Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2019-08-19 16:07+0800
PO-Revision-Date: 2019-08-17 20:24+0800
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: zh_Hant_TW
Language-Team: zh_Hant_TW <LL@li.org>
Plural-Forms: nplurals=1; plural=0
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.7.0
 關於我 或者，您可以在瀏覽器的地址欄中粘貼以下鏈接： 發生意外的錯誤 退回 查看您的電子郵件，了解重置密碼的說明 點擊註冊！ 點擊重置它 祝賀，帖子被創建！ 祝賀，帖子更新！ 恭喜，您現在是註冊用戶！ 內容 創建帖子 親愛的 %(username)s， 編輯 編輯帖子 編輯個人資料 編輯您的個人資料 電子郵件 需要電子郵件地址 瀏覽所有 探索頁面 文件未找到 關注 禁止 忘記密碼了嗎？ 嗨，%(username)s！ 主頁 主頁 如果你沒有要求密碼重置，只需忽略此消息。 用戶名或密碼無效 最後一次出現：%(when)s 登入 登出 微博 必須至少6個字符，不超過64個字符。 字串長度不超過140個字 最多64個字符 需要帖子標題。 需要密碼 需要用戶名。 最新帖子 新用戶？ 密碼 需要密碼 請輸入6~64個字符的用戶名。 請輸入電子郵件地址 請輸入長度為120的電子郵件地址。 請輸入密碼 請再次輸入密碼 請輸入帖子內容 請輸入長度為140的帖子內容。 請輸入帖子標題 請輸入長度為64的帖子標題。 請輸入自我內容 請輸入用戶名 請登錄訪問該頁面! 請使用其他電子郵件地址。 請使用其他用戶名。 帖子標題 個人檔案 註冊 記住賬號 重複輸入密碼 請求密碼重置 重設密碼 重置你的密碼 發送密碼重置電子郵件 登入 此致 提交 管理員已收到通知。很抱歉給您帶來不便！ 沒有用戶使用電子郵件，{0} 要重置密碼 取消關注 更新 用戶名 用戶名 需要用戶名 找不到用戶{0}。 用戶：%(username)s 歡迎來到微博 你正在關注{0} 你沒有關注{0} 你不能跟隨自己！ 你無法取消關注自己！ 您沒有訪問所請求資源的權限。它受到唯讀保護或服務器無法讀取。 你的修改已被保存。 您的密碼已重置。 點擊這裡 粉絲 追蹤 