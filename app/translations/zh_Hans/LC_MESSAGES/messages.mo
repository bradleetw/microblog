��    \      �              �     �  N   �      5     V  ;   [     �     �  !   �  !   �  /         0     8     D     W  	   \     f     s     �     �     �     �     �     �  	   �     �     �     �  	     F        T     q     �     �  	   �  5   �     �     �     	     #	     1	     A	  	   J	     T	     ]	  1   n	     �	  ,   �	     �	     �	     
  +   4
     `
  (   x
     �
     �
  "   �
  %   �
        
   <     G     O     X     d     t     �     �     �     �  
   �     �  A   �      %     F     ]     f  	   m  	   w     �     �     �     �     �     �     �       z   9     �     �  
   �  	   �  	     }    	   �  B   �     �     �  6   �     .     >     N     j  $   �     �     �     �     �     �     �     �               8     ?     L     \     c     j     �     �     �  <   �     �     �               &  -   .     \     x     �     �     �     �     �     �     �  %   �       3   0     d     t     �  '   �     �  &   �          !     4  $   N     s     �     �     �     �     �     �     �     �          "     )     0  9   7  /   q     �     �     �  	   �  	   �     �     �     �          (     ;     N     g  Z   �     �     �          #     *   About me Alternatively, you can paste the following link in your browser's address bar: An unexpected error has occurred Back Check your email for the instruction to reset your password Click to Register! Click to Reset It Congratulations, post is created! Congratulations, post is updated! Congratulations, you are now a registered user! Content Create Post Dear %(username)s, Edit Edit Post Edit Profile Edit your profile Email Email address is must Explore Explore Page File Not Found Follow Forbidden Forgot Your Password? Hi, %(username)s! Home Home Page If you have not requested a password reset simply ignore this message. Invalid username or password Last seen on: %(when)s Login Logout Microblog Must be at least 6 characters, at most 64 characters. Must be at most 140 characters Must be at most 64 characters Need Post title. Need password Need user name. New Post New User? Password Password is must Please input User name between 6 ~ 64 characters. Please input email address Please input email address under 120 length. Please input password Please input password again Please input post content Please input post content under 140 length. Please input post title Please input post title under 64 length. Please input self-content Please input user name Please log in to access this page! Please use a different email address. Please use a different username. Post Title Profile Register Remember Me Repeat Password Request Password Reset Reset Password Reset Your Password Send Password Reset e-mail Sign In Sincerely, Submit The administrator has been notified. Sorry for the inconvenience! There is no user with email, {0} To reset your password Unfollow Update User Name User name User name is must User {0} not found. User: %(username)s Welcome to Microblog You are following {0} You are not following {0} You cannot follow yourself! You cannot unfollow yourself! You don't have the permission to access the requested resource. It is either read-protected or not readable by the
server. Your changes have been saved. Your password has been reset. click here followers following Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2019-08-19 16:07+0800
PO-Revision-Date: 2019-08-17 16:51+0800
Last-Translator: 
Language: zh_Hans_CN
Language-Team: zh_Hans_CN <LL@li.org>
Plural-Forms: nplurals=1; plural=0
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.7.0
 关于我 或者，您可以在浏览器的地址栏中粘贴以下链接： 发生意外的错误 退回 查看您的电子邮件，了解重置密码的说明 点击注册！ 点击重置它 祝贺，帖子被创建！ 祝贺，帖子更新！ 恭喜，您现在是注册用户！ 内容 创建帖子 你好%(username)s， 编辑 编辑帖子 编辑个人资料 编辑您的个人资料 电子邮件 需要电子邮件地址 探索 探索页面 文件未找到 关注 禁止 忘记密码了吗？ 嗨，%(username)s！ 主页 主页 如果你没有要求密码重置，只需忽略此消息。 用户名或密码无效 最后一次出现：%(when)s 登录 登出 微博9 必须至少6个字符，最多64个字符。 必须不超过140个字符 最多64个字符 需要帖子标题。 需要密码 需要用户名。 最新帖子 新用户？ 密码 需要密码 请输入6~64个字符的用户名。 请输入电子邮件地址 请输入长度不超过120的电子邮件地址。 请输入密码 请再次输入密码 请输入帖子内容 请输入长度为140的帖子内容。 请输入帖子标题 请在64长度下输入帖子标题。 请输入自我相关内容 请输入用户名 请登录访问该页面! 请使用其他电子邮件地址。 请使用其他用户名。 帖子标题 个人资料 注册 记住账号 重复输入密码 请求密码重置 重设密码 重置你的密码 发送密码重置电子邮件 登入 此致 提交 管理员已收到通知。很抱歉给您带来不便！ {0}, 没有用该电子邮件注册的使用者 要重置密码 取消关注 更新 用户名 用户名 需要用户名 找不到用户{0}。 用户：%(username)s 欢迎来到微博 你正在关注{0} 你没有关注{0} 你不能关注自己！ 你无法取消关注自己！ 您没有访问所请求资源的权限。它受到唯读保护或服务器无法读取。 你的修改已被保存。 您的密码已重置。 点击这里 粉丝 关注 